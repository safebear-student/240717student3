import base_test


class TestCases(base_test.BaseTest):
    def test_01_test_login_page_login(self):
        #Step 1 : Clikc on login and the login page loads
        assert self.welcomepage.click_login(self.loginpage)
        #Step 2 : Login to the webpage and confirm the Main page appears
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        assert self.mainpage.click_logout(self.welcomepage)
